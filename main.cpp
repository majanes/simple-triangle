#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include <SDL.h> // For Events
#include <assert.h>
#include <inttypes.h>
#include <queue>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <vector>

#define GL_DECL(T_ID, ID) static T_ID ID ## _p = NULL
GL_DECL( PFNGLACTIVETEXTUREPROC, glActiveTexture);
GL_DECL( PFNGLATTACHSHADERPROC, glAttachShader);
GL_DECL( PFNGLBINDATTRIBLOCATIONPROC, glBindAttribLocation);
GL_DECL( PFNGLBINDBUFFERPROC, glBindBuffer);
GL_DECL( PFNGLBINDTEXTUREPROC, glBindTexture);
GL_DECL( PFNGLBUFFERDATAPROC, glBufferData);
GL_DECL( PFNGLCLEARCOLORPROC, glClearColor);
GL_DECL( PFNGLCLEARPROC, glClear);
GL_DECL( PFNGLCOMPILESHADERPROC, glCompileShader);
GL_DECL( PFNGLCREATEPROGRAMPROC, glCreateProgram);
GL_DECL( PFNGLCREATESHADERPROC, glCreateShader);
GL_DECL( PFNGLDRAWARRAYSPROC, glDrawArrays);
GL_DECL( PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray);
GL_DECL( PFNGLGENBUFFERSPROC, glGenBuffers);
GL_DECL( PFNGLGENTEXTURESPROC, glGenTextures);
GL_DECL( PFNGLGETATTRIBLOCATIONPROC, glGetAttribLocation);
GL_DECL( PFNGLGETERRORPROC, glGetError);
GL_DECL( PFNGLGETFIRSTPERFQUERYIDINTELPROC, glGetFirstPerfQueryIdINTEL);
GL_DECL( PFNGLGETNEXTPERFQUERYIDINTELPROC, glGetNextPerfQueryIdINTEL);
GL_DECL( PFNGLGETPERFQUERYINFOINTELPROC, glGetPerfQueryInfoINTEL);
GL_DECL( PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog);
GL_DECL( PFNGLGETPROGRAMIVPROC, glGetProgramiv);
GL_DECL( PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog);
GL_DECL( PFNGLGETSHADERIVPROC, glGetShaderiv);
GL_DECL( PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation);
GL_DECL( PFNGLLINKPROGRAMPROC, glLinkProgram);
GL_DECL( PFNGLSHADERSOURCEPROC, glShaderSource);
GL_DECL( PFNGLTEXIMAGE2DPROC, glTexImage2D);
GL_DECL( PFNGLTEXPARAMETERIPROC, glTexParameteri);
GL_DECL( PFNGLUNIFORM1IPROC, glUniform1i);
GL_DECL( PFNGLUSEPROGRAMPROC, glUseProgram);
GL_DECL( PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer);
GL_DECL(PFNGLBEGINPERFQUERYINTELPROC, glBeginPerfQueryINTEL);
GL_DECL(PFNGLCREATEPERFQUERYINTELPROC, glCreatePerfQueryINTEL);
GL_DECL(PFNGLENDPERFQUERYINTELPROC, glEndPerfQueryINTEL);
GL_DECL(PFNGLGETPERFCOUNTERINFOINTELPROC, glGetPerfCounterInfoINTEL);
GL_DECL(PFNGLGETPERFQUERYDATAINTELPROC, glGetPerfQueryDataINTEL);

#define GL_DEF(T_ID, ID)  \
  assert(ID##_p == NULL); \
  ID##_p = (T_ID) SDL_GL_GetProcAddress(#ID);    \
  assert(ID##_p != NULL);

static void
init_dispatch() {
  GL_DEF(PFNGLACTIVETEXTUREPROC, glActiveTexture);
  GL_DEF(PFNGLATTACHSHADERPROC, glAttachShader);
  GL_DEF(PFNGLBEGINPERFQUERYINTELPROC, glBeginPerfQueryINTEL);
  GL_DEF(PFNGLBINDATTRIBLOCATIONPROC, glBindAttribLocation);
  GL_DEF(PFNGLBINDBUFFERPROC, glBindBuffer);
  GL_DEF(PFNGLBINDTEXTUREPROC, glBindTexture);
  GL_DEF(PFNGLBUFFERDATAPROC, glBufferData);
  GL_DEF(PFNGLCLEARCOLORPROC, glClearColor);
  GL_DEF(PFNGLCLEARPROC, glClear);
  GL_DEF(PFNGLCOMPILESHADERPROC, glCompileShader);
  GL_DEF(PFNGLCREATEPERFQUERYINTELPROC, glCreatePerfQueryINTEL);
  GL_DEF(PFNGLCREATEPROGRAMPROC, glCreateProgram);
  GL_DEF(PFNGLCREATESHADERPROC, glCreateShader);
  GL_DEF(PFNGLDRAWARRAYSPROC, glDrawArrays);
  GL_DEF(PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray);
  GL_DEF(PFNGLENDPERFQUERYINTELPROC, glEndPerfQueryINTEL);
  GL_DEF(PFNGLGENBUFFERSPROC, glGenBuffers);
  GL_DEF(PFNGLGENTEXTURESPROC, glGenTextures);
  GL_DEF(PFNGLGETATTRIBLOCATIONPROC, glGetAttribLocation);
  GL_DEF(PFNGLGETERRORPROC, glGetError);
  GL_DEF(PFNGLGETFIRSTPERFQUERYIDINTELPROC, glGetFirstPerfQueryIdINTEL);
  GL_DEF(PFNGLGETNEXTPERFQUERYIDINTELPROC, glGetNextPerfQueryIdINTEL);
  GL_DEF(PFNGLGETPERFCOUNTERINFOINTELPROC, glGetPerfCounterInfoINTEL);
  GL_DEF(PFNGLGETPERFQUERYDATAINTELPROC, glGetPerfQueryDataINTEL);
  GL_DEF(PFNGLGETPERFQUERYINFOINTELPROC, glGetPerfQueryInfoINTEL);
  GL_DEF(PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog);
  GL_DEF(PFNGLGETPROGRAMIVPROC, glGetProgramiv);
  GL_DEF(PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog);
  GL_DEF(PFNGLGETSHADERIVPROC, glGetShaderiv);
  GL_DEF(PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation);
  GL_DEF(PFNGLLINKPROGRAMPROC, glLinkProgram);
  GL_DEF(PFNGLSHADERSOURCEPROC, glShaderSource);
  GL_DEF(PFNGLTEXIMAGE2DPROC, glTexImage2D);
  GL_DEF(PFNGLTEXPARAMETERIPROC, glTexParameteri);
  GL_DEF(PFNGLUNIFORM1IPROC, glUniform1i);
  GL_DEF(PFNGLUSEPROGRAMPROC, glUseProgram);
  GL_DEF(PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer);
}

SDL_Window *createWindow(void) {
  int screenwidth  = 800;
  int screenheight = 800;
  SDL_Init(SDL_INIT_VIDEO);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_Window *glesWindow = SDL_CreateWindow("Simple Triangle",
                                            0, 0, screenwidth, screenheight,
                                            SDL_WINDOW_OPENGL);
  if (! glesWindow) {
    printf("could not open window: %s\n", SDL_GetError());
    return NULL;
  }
  SDL_GLContext glcontext = SDL_GL_CreateContext( glesWindow );
  SDL_GL_MakeCurrent(glesWindow, glcontext);
  return glesWindow;
}


static void
init_triangle() {
  GLuint array_buffer;
  assert(glGetError_p() == GL_NO_ERROR);
  glGenBuffers_p(1, &array_buffer);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindBuffer_p(GL_ARRAY_BUFFER, array_buffer);
  assert(glGetError_p() == GL_NO_ERROR);
  GLuint vert_shader = glCreateShader_p(GL_VERTEX_SHADER);
  assert(glGetError_p() == GL_NO_ERROR);
  const GLchar *vert_src = "#version 100\n"
                           "precision mediump float;"
                           "attribute vec2 coord2d;"
                           "varying vec2 v_TexCoordinate;"
                           "void main(void) {"
                           "  gl_Position = vec4(coord2d.x, coord2d.y, 0, 1);"
                           "  v_TexCoordinate = vec2(coord2d.x, coord2d.y);"
                           "}";
  glShaderSource_p(vert_shader, 1,
                 &vert_src,
                 NULL);
  assert(glGetError_p() == GL_NO_ERROR);
  glCompileShader_p(vert_shader);
  assert(glGetError_p() == GL_NO_ERROR);
  GLint status;
  glGetShaderiv_p(vert_shader, GL_COMPILE_STATUS, &status);
  assert(glGetError_p() == GL_NO_ERROR);
  if (status == GL_FALSE) {
    GLsizei length;
    GLchar log[1024];
    glGetShaderInfoLog_p(vert_shader, 1024, &length, log);
    assert(glGetError_p() == GL_NO_ERROR);
    printf("%s\n", log);
  }

  GLuint frag_shader = glCreateShader_p(GL_FRAGMENT_SHADER);
  assert(glGetError_p() == GL_NO_ERROR);
  const GLchar *frag_src = "#version 100\n"
                           "precision mediump float;"
                           "uniform sampler2D texUnit;"
                           "varying vec2 v_TexCoordinate;"
                           "void main(void) {"
                           "  gl_FragColor = texture2D(texUnit, v_TexCoordinate);"
                           "}";
  glShaderSource_p(frag_shader, 1, &frag_src, NULL);
  assert(glGetError_p() == GL_NO_ERROR);
  glCompileShader_p(frag_shader);
  assert(glGetError_p() == GL_NO_ERROR);
  glGetShaderiv_p(frag_shader, GL_COMPILE_STATUS, &status);
  if (status != GL_TRUE) {
    GLsizei length;
    GLchar log[1024];
    glGetShaderInfoLog_p(frag_shader,  1024,  &length,  log);
    printf("%s\n", log);
  }
  assert(glGetError_p() == GL_NO_ERROR);

  GLuint program = glCreateProgram_p();
  glAttachShader_p(program, vert_shader);
  assert(glGetError_p() == GL_NO_ERROR);
  glAttachShader_p(program, frag_shader);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindAttribLocation_p(program, 0, "coord2d");
  glLinkProgram_p(program);
  glGetProgramiv_p(program, GL_LINK_STATUS, &status);
  if (status != GL_TRUE) {
    GLsizei length;
    GLchar log[1024];
    glGetProgramInfoLog_p(program,  1024,  &length,  log);
    printf("%s\n", log);
  }
  assert(glGetError_p() == GL_NO_ERROR);
  GLuint coord_loc = glGetAttribLocation_p(program, "coord2d");
  status = glGetError_p();
  if (status != GL_NO_ERROR) {
    printf("%x\n", status);
  }
  GLuint tex_loc = glGetUniformLocation_p(program, "texUnit");
  assert(glGetError_p() == GL_NO_ERROR);
  glBindBuffer_p(GL_ARRAY_BUFFER, array_buffer);
  assert(glGetError_p() == GL_NO_ERROR);
  GLfloat vert_data[] = { 0.0, 0.0,
                          0.0, 1.0,
                          1.0, 0.0};
  glBufferData_p(GL_ARRAY_BUFFER, sizeof(vert_data), vert_data, GL_STATIC_DRAW);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindBuffer_p(GL_ARRAY_BUFFER, 0);
  assert(glGetError_p() == GL_NO_ERROR);
  glUseProgram_p(program);
  assert(glGetError_p() == GL_NO_ERROR);
  /* glEnable(GL_TEXTURE_2D); */
  /* assert(glGetError_p() == GL_NO_ERROR); */
  GLuint texture;
  glGenTextures_p(1, &texture);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindTexture_p(GL_TEXTURE_2D, texture);
  assert(glGetError_p() == GL_NO_ERROR);
  glActiveTexture_p(GL_TEXTURE0);
  assert(glGetError_p() == GL_NO_ERROR);
  glTexParameteri_p(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  assert(glGetError_p() == GL_NO_ERROR);
  glTexParameteri_p(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  assert(glGetError_p() == GL_NO_ERROR);
  unsigned char pixels[] = { 0xFF, 0x00, 0x00, 0x00,
                             0x00, 0xFF, 0x00, 0x00,
                             0x00, 0x00, 0xFF, 0x00,
                             0xFF, 0xFF, 0xFF, 0x00 };
  glTexImage2D_p(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0,
               GL_RGBA, GL_UNSIGNED_BYTE,
               pixels);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindTexture_p(GL_TEXTURE_2D, texture);
  assert(glGetError_p() == GL_NO_ERROR);
  /* glEnable(GL_TEXTURE_2D); */
  /* assert(glGetError_p() == GL_NO_ERROR); */
  glClearColor_p(1,1,1,1);
  assert(glGetError_p() == GL_NO_ERROR);
  glClear_p(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
  assert(glGetError_p() == GL_NO_ERROR);
  glUseProgram_p(program);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindBuffer_p(GL_ARRAY_BUFFER, array_buffer);
  assert(glGetError_p() == GL_NO_ERROR);
  glEnableVertexAttribArray_p(coord_loc);
  assert(glGetError_p() == GL_NO_ERROR);
  glVertexAttribPointer_p(coord_loc, 2, GL_FLOAT, GL_FALSE, 0, NULL);
  assert(glGetError_p() == GL_NO_ERROR);
  glActiveTexture_p(GL_TEXTURE0);
  assert(glGetError_p() == GL_NO_ERROR);
  glBindTexture_p(GL_TEXTURE_2D, texture);
  assert(glGetError_p() == GL_NO_ERROR);
  glUniform1i_p(tex_loc, 0);
  assert(glGetError_p() == GL_NO_ERROR);

}

class Perf {
 public:
  Perf(const char *group_name, const char *metric_name) {
    glGetFirstPerfQueryIdINTEL_p(&m_query_id);
    static const int kMaxName = 128;
    GLchar current_name[kMaxName];
    GLuint data_size, number_counters, number_instances, capabilities_mask;
    while (m_query_id != 0) {
      glGetPerfQueryInfoINTEL_p(m_query_id,
                                kMaxName, current_name,
                                &data_size, &number_counters,
                                &number_instances, &capabilities_mask);
      printf("Group %d: %s\n", m_query_id, current_name);
      if (strstr(current_name, group_name))
        break;
      glGetNextPerfQueryIdINTEL_p(m_query_id, &m_query_id);
    }
    if (m_query_id == 0) {
      printf("group not found: %s\n", group_name);
      exit(-1);
    }
    printf("Group %d: %s\n", m_query_id, current_name);
    m_buf.resize(data_size);
    for (unsigned int counter_num = 1; counter_num <= number_counters;
         ++counter_num) {
      GLuint data_size, type;
      GLuint64 max_value;
      glGetPerfCounterInfoINTEL_p(m_query_id, counter_num,
                                  kMaxName, current_name,
                                  0, NULL,
                                  &m_offset, &data_size, &type,
                                  &m_data_type, &max_value);
      printf("metric %d: %s\n", counter_num, current_name);
      if (strstr(current_name, metric_name))
        break;
      m_offset = -1;
    }
    if (m_offset == (GLuint)-1) {
      printf("metric not found: %s\n", metric_name);
      exit(-1);
    }
  }
  void begin() {
    GLuint query_handle;
    if (m_free_handles.empty()) {
      glCreatePerfQueryINTEL_p(m_query_id, &query_handle);
      m_free_handles.push_back(query_handle);
    }
    query_handle = m_free_handles.back();
    m_free_handles.pop_back();
    glBeginPerfQueryINTEL_p(query_handle);
    assert(glGetError_p() == GL_NO_ERROR);
    m_active_handles.push(query_handle);
  }
  void end() {
    glEndPerfQueryINTEL_p(m_active_handles.back());
    while(!m_active_handles.empty()) {
      GLuint bytes_written;
      glGetPerfQueryDataINTEL_p(m_active_handles.front(),
                                GL_PERFQUERY_DONOT_FLUSH_INTEL,
                                m_buf.size(), m_buf.data(),
                                &bytes_written);
      if (!bytes_written)
              break;
      // else
      print();
      m_free_handles.push_back(m_active_handles.front());
      m_active_handles.pop();
    }
  }
 private:
  void print() {
    switch (m_data_type) {
      case GL_PERFQUERY_COUNTER_DATA_UINT32_INTEL: {
        const uint32_t val = *reinterpret_cast<const uint32_t *>(m_buf.data() + m_offset);
        printf("%" PRIu32 "\n", val);
        break;
      }
      case GL_PERFQUERY_COUNTER_DATA_UINT64_INTEL: {
        const uint64_t val = *reinterpret_cast<const uint64_t *>(m_buf.data() + m_offset);
        printf("%" PRIu64 "\n", val);
        break;
      }
      case GL_PERFQUERY_COUNTER_DATA_FLOAT_INTEL: {
        const float val = *reinterpret_cast<const float *>(m_buf.data() + m_offset);
        printf("%f\n", val);
        break;
      }
      case GL_PERFQUERY_COUNTER_DATA_DOUBLE_INTEL: {
        const double val = *reinterpret_cast<const double *>(m_buf.data() + m_offset);
        printf("%f\n", val);
        break;
      }
      case GL_PERFQUERY_COUNTER_DATA_BOOL32_INTEL: {
        const bool val = *reinterpret_cast<const bool*>(m_buf.data() + m_offset);
        printf("%s\n", val ? "true" : "false");
        break;
      }
      default:
        assert(false);
    }
  }
  
  GLuint m_query_id;
  std::vector<GLuint> m_free_handles;
  std::queue<GLuint> m_active_handles;
  std::vector<unsigned char> m_buf;
  GLuint m_offset, m_data_type;
};

#ifdef _WIN32
int WinMain()
#else
int main()
#endif
{
  SDL_Window* window = createWindow();
  if (! window)
    return -1;
  SDL_GL_LoadLibrary(NULL);
  init_dispatch();
  Perf p("Render Metrics Basic", "Core");
  bool quit = false;
  bool blue_color = false;
  SDL_Event e;
  init_triangle();
  while (! quit) {
    while( SDL_PollEvent( &e ) != 0 ) {
      if( e.type == SDL_QUIT ) {
        quit = true;
      }
    }
    if (blue_color) 
      glClearColor_p(0.0, 0.0, 1.0, 1.0);
    else
      glClearColor_p(0.0, 1.0, 0.0, 1.0);
    blue_color = !blue_color;
    
    p.begin();
    glClear_p(GL_COLOR_BUFFER_BIT);
    p.end();

    p.begin();
    glDrawArrays_p(GL_TRIANGLES, 0, 3);
    p.end();

    SDL_GL_SwapWindow( window );
  }

  return 0;
}
