To build:

install python3, then:

    > pip3 install meson
    > git clone https://gitlab.freedesktop.org/majanes/simple-triangle.git
    > cd simple-triangle
    > "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x64
    > meson build --backend vs2015

   - open visual studio solution in simple-triangle/build
   - build
   - download https://libsdl.org/release/SDL2-2.0.9-win32-x64.zip and put sdl DLL in build/
   - debug

